# Kubernetes Deployment Instruction

```bash
kubectl apply -f config.yml
kubectl apply -f secret.yml
kubectl apply -f ingress-resource-sportsbook.yml
kubectl apply -f postgres.yml
kubectl apply -f prometheus-config.yml
kubectl apply -f prometheus.yml
kubectl apply -f grafana.yml
kubectl apply -f sportsbook.yml
```

## Sportsbook

The sportsbook image will do the migration and import included `Data.csv` to the database
See [boot.sh](../boot.sh).

![Migration and Import Data](./sportsbook-migration.jpg)

----

### Checking prometheus target
![Prometheus](./prometheus.jpg)

----

### Adding prometheus Data source to grafana
![Grafana](./grafana.jpg)

----

### Sample raph data from Prmetheus Data source
![Telemetry Json/Protocol Buffer request](./grafana-api-request.jpg)

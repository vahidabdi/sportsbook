defmodule Sportsbook.Repo.Migrations.CreateMatch do
  use Ecto.Migration

  def change do
    create table(:matches) do
      add(:home_team, :text, null: false)
      add(:away_team, :text, null: false)
      add(:date, :date, null: false)
      add(:season_id, references(:season), null: false)
      add(:fthg, :smallint, null: false)
      add(:ftag, :smallint, null: false)
      add(:hthg, :smallint, null: false)
      add(:htag, :smallint, null: false)
      add(:ftr, :string, size: 1, null: false)
      add(:htr, :string, size: 1, null: false)
    end

    create constraint(:matches, :fthg_must_br_gq_zero, check: "fthg >= 0")
    create constraint(:matches, :ftag_must_br_gq_zero, check: "ftag >= 0")
    create constraint(:matches, :hthg_must_br_gq_zero, check: "hthg >= 0")
    create constraint(:matches, :htag_must_br_gq_zero, check: "htag >= 0")
    create constraint(:matches, :ftr_inclusion, check: "ftr IN ('A', 'D', 'H')")
    create constraint(:matches, :htr_inclusion, check: "htr IN ('A', 'D', 'H')")
    create index(:matches, [:home_team, :away_team, :season_id, :date], unique: true, name: :unique_match)
  end
end

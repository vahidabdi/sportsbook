defmodule Sportsbook.Repo.Migrations.CreateLeague do
  use Ecto.Migration

  def change do
    create table(:leagues) do
      add(:name, :text, null: false)
    end

    create index(:leagues, :name, unique: true)
  end
end

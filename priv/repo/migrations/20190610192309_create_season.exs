defmodule Sportsbook.Repo.Migrations.CreateSeason do
  use Ecto.Migration

  def change do
    create table(:season) do
      add(:season_name, :text, null: false)
      add(:league_id, references(:leagues), null: false)
    end

    create index(:season, [:league_id, :season_name], unique: true, name: :unqiue_season)
  end
end

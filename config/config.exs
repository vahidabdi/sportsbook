# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :sportsbook,
  ecto_repos: [Sportsbook.Repo]

# Configures the endpoint
config :sportsbook, SportsbookWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "HMwnAOS+/aQo3LStWyCCYP+dQytrq00XlM9lSAK5XY4ozIg9Z4CNjOdd+xb6Ajbp",
  render_errors: [view: SportsbookWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Sportsbook.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :mime, :types, %{
  "application/x-protobuf" => ["protobuf"]
}

config :plug, :formats, %{
  "application/json" => [:json],
  "application/x-protobuf" => [:protobuf]
}

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :sportsbook, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      router: SportsbookWeb.Router,     # phoenix routes will be converted to swagger paths
      endpoint: SportsbookWeb.Endpoint  # (optional) endpoint config used to set host, port and https schemes.
    ]
  }

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

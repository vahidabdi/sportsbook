defmodule Mix.Tasks.Sportsbook.ImportData do
  use Mix.Task

  @moduledoc """
  Import Data.csv into the corresponding tables
  """

  @shortdoc "Import data from CSV file"

  require Logger

  alias Sportsbook.Repo
  alias Sportsbook.Competition
  alias Sportsbook.Competition.{League, Season, Match}

  @start_apps [
    :postgrex,
    :ecto
  ]
  @app :sportsbook

  def repos, do: Application.get_env(@app, :ecto_repos, [])

  def run([csv_file]) do
    Logger.configure(level: :info)
    Application.ensure_all_started(:ecto_sql)

    Application.load(@app)
    Enum.each(@start_apps, &Application.ensure_all_started/1)
    Enum.each(repos(), & &1.start_link(pool_size: 2))

    case File.read(csv_file) do
      {:ok, content} ->
        process_csv(content)
      {:error, _} ->
        Logger.error("Cannot open the file")
    end
  end
  def run(_) do
    Logger.error("argument error: mix sportsbook.import_data Data.csv")
  end

  defp process_csv(content) do
    Logger.info("Processing CSV...")
    lines = String.split(content, "\r\n", trim: true)

    result =
      lines
      |> Stream.drop(1) # Ignore header
      |> Enum.reduce(%{cache: %{}, inserts: []}, fn line, acc ->
        fields = String.split(line, ",") # Split the fields in each line
        fields_to_keyword(fields, acc)
      end)

    {count, _} = Repo.insert_all(Match, result.inserts, on_conflict: :nothing)
    Logger.info("#{count} match inserted")
  end

  defp fields_to_keyword(fields, acc) do
    [_id, div, season, date, ht, at, fthg, ftag, ftr, hthg, htag, htr] = fields

    {_div_id, cache} = insert_or_get_div(div, acc.cache)
    {season_id, cache} = insert_or_get_season(season, div, cache)

    [d,m,y] = String.split(date, "/") |> Enum.map(&String.to_integer/1)
    {:ok, date} = Date.new(y,m,d)

    insert_row = [
      season_id: season_id,
      date: date,
      home_team: ht,
      away_team: at,
      fthg: String.to_integer(fthg),
      ftag: String.to_integer(ftag),
      ftr: ftr,
      hthg: String.to_integer(hthg),
      htag: String.to_integer(htag),
      htr: htr
    ]
    %{cache: cache, inserts: [insert_row | acc.inserts]}
  end

  defp insert_or_get_div(div, cache) do
    case Map.get(cache, div) do
      nil ->
        div_id =
          case Repo.get_by(League, name: div) do
            %League{id: league_id} ->
              league_id

            nil ->
              insert_div(div)
          end
        new_cache = Map.put_new(cache, div, div_id)
        {div_id, new_cache}

      div_id -> {div_id, cache}
    end
  end

  defp insert_or_get_season(season, div, cache) do
    season_key = "#{div}_#{season}"
    case Map.get(cache, season_key) do
      nil ->
        %League{id: league_id} = Repo.get_by(League, name: div)

        season_id =
          case Repo.get_by(Season, %{season_name: season, league_id: league_id}) do
            %Season{id: season_id} ->
              season_id

            nil ->
              insert_season(div, season)
          end

        new_cache = Map.put_new(cache, season_key, season_id)
        {season_id, new_cache}

      season_id -> {season_id, cache}
    end
  end

  defp insert_div(div_name) do
    cs = Competition.create_league(%{name: div_name})
    %League{id: id} = Sportsbook.Repo.insert!(cs)
    id
  end
  defp insert_season(div_name, season_name) do
    %League{id: league_id} = Sportsbook.Repo.get_by!(League, name: div_name)
    cs = Competition.create_season(%{season_name: season_name, league_id: league_id})
    %Season{id: id} = Sportsbook.Repo.insert!(cs)
    id
  end
end

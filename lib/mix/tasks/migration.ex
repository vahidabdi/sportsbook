defmodule Sportsbook.ReleaseTasks do
  @start_apps [
    :postgrex,
    :ecto
  ]
  @app :sportsbook

  def repos, do: Application.get_env(@app, :ecto_repos, [])

  def migrate do
    prepare()
    Enum.each(repos(), &run_migrations_for/1)
  end

  defp run_migrations_for(repo) do
    Keyword.get(repo.config, :otp_app)
    migrations_path = priv_path_for(repo, "migrations")
    Ecto.Migrator.run(repo, migrations_path, :up, all: true)
  end

  defp prepare do
    # Force start Ecto SQL apps
    {:ok, _} = Application.ensure_all_started(:ecto_sql)


    case Application.load(@app) do
      :ok -> :ok
      {:error, {:already_loaded, @app}} -> :ok
      err -> raise "Unknown state for application load: #{inspect(err)}"
    end

    IO.puts("Starting dependencies..")
    # Start apps necessary for executing migrations
    Enum.each(@start_apps, &Application.ensure_all_started/1)

    # Start the Repo(s)
    IO.puts("Starting repos..")
    Enum.each(repos(), & &1.start_link(pool_size: 2))
  end

  defp priv_path_for(repo, filename) do
    app = Keyword.get(repo.config(), :otp_app)

    repo_underscore =
      repo
      |> Module.split()
      |> List.last()
      |> Macro.underscore()

    priv_dir = "#{:code.priv_dir(app)}"

    Path.join([priv_dir, repo_underscore, filename])
  end
end

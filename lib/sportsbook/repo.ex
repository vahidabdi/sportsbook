defmodule Sportsbook.Repo do
  use Ecto.Repo,
    otp_app: :sportsbook,
    adapter: Ecto.Adapters.Postgres
  use Scrivener, page_size: 10
end

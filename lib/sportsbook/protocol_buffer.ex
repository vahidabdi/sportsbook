defmodule Sportsbook.ProtocolBuffer do
  use Protobuf, from: Path.join(File.cwd!, "priv/proto/league.proto")

  alias Sportsbook.ProtocolBuffer
  alias Sportsbook.Competition

  def from_model([]) do
    ProtocolBuffer.LeagueItems.new()
  end
  def from_model([_|_] = leagues) do
    leagues = Enum.map(leagues, &from_model/1)
    ProtocolBuffer.LeagueItems.new(leagues: leagues)
  end
  def from_model(%Competition.League{} = league) do
    league_proto =
      case league.season do
        %Ecto.Association.NotLoaded{} ->
          Map.drop(league, [:__meta__, :__struct__, :season])
        _ ->
          put_assoc(league, league.season, :season)
      end

    ProtocolBuffer.League.new(league_proto)
  end
  def from_model(%Competition.Season{} = season) do
    season_proto =
      case season.matches do
        %Ecto.Association.NotLoaded{} ->
          Map.drop(season, [:__meta__, :__struct__, :matches])
        _ ->
          put_assoc(season, season.matches, :matches)
      end

    ProtocolBuffer.Season.new(season_proto)
  end
  def from_model(%Competition.Match{} = match) do
    match
    |> Map.drop([:__meta__, :__struct__, :season])
    |> Map.put(:date, Date.to_string(match.date))
    |> Map.put(:htr, String.to_existing_atom(match.htr))
    |> Map.put(:ftr, String.to_existing_atom(match.ftr))
    |> ProtocolBuffer.Match.new()
  end

  defp put_assoc(map, assoc, assoc_key) do
    assoc_proto = Enum.map(assoc, &from_model/1)
    map
    |> Map.drop([:__meta__, :__struct__, assoc_key])
    |> Map.put(assoc_key, assoc_proto)
  end
end

defmodule Sportsbook.Competition.Match do
  use Ecto.Schema

  import Ecto.Changeset

  alias Sportsbook.Competition.Season

  @required_fields [:home_team, :away_team, :date, :fthg, :ftag, :hthg, :htag, :ftr, :htr]

  @derive {Jason.Encoder, except: [:__meta__, :__struct__, :season]}
  schema("matches") do
    field(:home_team, :string)
    field(:away_team, :string)
    field(:date, :date)
    field(:fthg, :integer)
    field(:ftag, :integer)
    field(:hthg, :integer)
    field(:htag, :integer)
    field(:ftr, :string, size: 1)
    field(:htr, :string, size: 1)

    belongs_to(:season, Season)
  end

  def changeset(match, params \\ %{}) do
    match
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> assoc_constraint(:season)
    |> validate_inclusion(:ftr, ["A", "D", "H"])
    |> validate_inclusion(:htr, ["A", "D", "H"])
    |> unique_constraint(:home_team, name: :unique_match)
  end
end


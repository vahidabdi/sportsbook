defmodule Sportsbook.Competition do

  import Ecto.Query

  alias Sportsbook.Repo
  alias Sportsbook.Competition.League
  alias Sportsbook.Competition.Season
  alias Sportsbook.Competition.Match


  def create_league(params \\ %{}) do
    League.changeset(%League{}, params)
  end

  def list_leagues_query() do
    from(l in League, preload: :season)
  end

  def list_leagues() do
    list_leagues_query() |> Repo.all()
  end

  def list_matches_query(season_id) do
    from(m in Match, where: m.season_id == ^season_id)
  end

  def list_matches(season_id) do
    list_matches_query(season_id) |> Repo.all()
  end

  def create_season(params \\ %{}) do
    Season.changeset(%Season{}, params)
  end

  def create_match(params \\ %{}) do
    Match.changeset(%Match{}, params)
  end
end

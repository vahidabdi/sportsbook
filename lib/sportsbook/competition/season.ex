defmodule Sportsbook.Competition.Season do
  use Ecto.Schema

  import Ecto.Changeset

  alias Sportsbook.Competition.League
  alias Sportsbook.Competition.Match

  @derive {Jason.Encoder, except: [:__meta__, :__struct__, :league, :matches]}
  schema("season") do
    field(:season_name, :string)

    belongs_to(:league, League)
    has_many(:matches, Match)
  end

  def changeset(season, params \\ %{}) do
    season
    |> cast(params, [:season_name, :league_id])
    |> validate_required([:season_name, :league_id])
    |> assoc_constraint(:league)
    |> unique_constraint(:season_name, name: :unique_season)
  end
end


defmodule Sportsbook.Competition.League do
  use Ecto.Schema

  import Ecto.Changeset

  alias Sportsbook.Competition.Season

  @derive {Jason.Encoder, except: [:__meta__, :__struct__, :season]}
  schema("leagues") do
    field(:name, :string)

    has_many(:season, Season)
  end

  def changeset(league, params \\ %{}) do
    league
    |> cast(params, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end


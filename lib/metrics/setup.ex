defmodule Metrics.Setup do
  def setup do
    Metrics.PlugInstrumenter.setup()

    SportsbookWeb.MetricsExporter.setup()
  end
end

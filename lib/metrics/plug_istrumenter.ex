defmodule Metrics.PlugInstrumenter do
  use Prometheus.Metric

  def setup() do
    Counter.declare(
      name: :web_request_json_count,
      help: "Number of json api call"
    )
    Counter.declare(
      name: :web_request_protobuf_count,
      help: "Number of protocol buffer api call"
    )

    :telemetry.attach("json-api-counter", [:web, :request, :stop], &handle_event/4, %{})
  end

  def handle_event([:web, :request, :stop], measurement, metadata, config) do
    case metadata.conn.assigns.format do
      :protobuf ->
        Counter.inc(name: :web_request_protobuf_count)
      :json ->
        Counter.inc(name: :web_request_json_count)
    end
  end
end

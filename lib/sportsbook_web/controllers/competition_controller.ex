defmodule SportsbookWeb.CompetitionController do
  use SportsbookWeb, :controller
  use PhoenixSwagger

  import Plug.Conn

  action_fallback SportsbookWeb.FallbackController

  alias Sportsbook.{Repo, Competition}
  alias Sportsbook.ProtocolBuffer
  alias SportsbookWeb.{LeagueView, MatchView}

  swagger_path :index do
    get "/api/competitions"
    summary "List all the competitions"
    description "List all the available leagues with their season"
    tag "Competitions"
    paging
    response 200, "Success", Schema.ref(:Leagues)
  end
  def index(%{assigns: %{format: :json}} = conn, params) do
    page = Competition.list_leagues_query() |> Repo.paginate(params)

    conn
    |> put_view(LeagueView)
    |> render("index.json", %{
          leagues: page.entries,
          page_number: page.page_number,
          page_size: page.page_size,
          total_pages: page.total_pages,
          total_entries: page.total_entries
        })
  end
  def index(%{assigns: %{format: :protobuf}} = conn, _params) do
    response_content =
      Competition.list_leagues()
      |> ProtocolBuffer.from_model()
      |> ProtocolBuffer.LeagueItems.encode()

    conn
    |> put_resp_content_type("x-protobuf")
    |> send_resp(200, response_content)
  end

  swagger_path :show do
    get "/api/competitions/{season_id}"
    summary "List all the matches"
    description "List all the matches for specific season"
    parameters do
      season_id :path, :integer, "The season ID", required: true
    end
    tag "Matches"
    paging
    response 200, "Success", Schema.ref(:Matches)
    response 404, "Not Found", Schema.ref(:Error)
  end
  def show(%{assigns: %{format: :json}} = conn, %{"season_id" => season_id} = params) do
    page = Competition.list_matches_query(season_id) |> Repo.paginate(params)
    case page do
      [] ->
        {:error, :season_not_found}
      _ ->
        conn
        |> put_view(MatchView)
        |> render("index.json", %{
              matches: page.entries,
              page_number: page.page_number,
              page_size: page.page_size,
              total_pages: page.total_pages,
              total_entries: page.total_entries
            })
    end
  end
  def show(%{assigns: %{format: :protobuf}} = conn, %{"season_id" => season_id}) do
    matches = Competition.list_matches(season_id)
    response_content =
      case matches do
        [] ->
          ProtocolBuffer.MatchItems.new()
        _ ->
          ProtocolBuffer.from_model(matches)
      end
      |> ProtocolBuffer.MatchItems.encode()

    conn
    |> put_resp_content_type("x-protobuf")
    |> send_resp(200, response_content)
  end

  def swagger_definitions do
    %{
      Match: swagger_schema do
        title "Match"
        description "A match"
        properties do
          id :integer, "match ID"
          home_team :string, "Home team name"
          away_team :string, "Away team name"
          date :string, "Date of the match in YYYY-MM-DD format"
          fthg :integer, "Final home team score"
          ftag :integer, "Final away team score"
          hthg :integer, "Half time home team score"
          htag :integer, "Half time away team score"
          ftr :string, "Final match score", enum: ["A", "D", "H"]
          htr :string, "Half time match score", enum: ["A", "D", "H"]
        end
      end,
      Matches: swagger_schema do
        title "Matches"
        description "A collection of Matches"
        type :array
        items Schema.ref(:Match)
        example %{
          data: [
            %{
              id: 1,
              home_team: "Barcelona",
              away_team: "Real Madrid",
              date: "2016-12-03",
              fthg: 1,
              ftag: 1,
              ftr: "D",
              hthg: 0,
              htag: 0,
              htr: "D"
            }
          ]
        }
      end,
      Season: swagger_schema do
        title "Season"
        description "A season"
        properties do
          id :integer, "Season ID"
          season_name :string, "Name of season e.g. 201617"
        end
      end,
      Seasons: swagger_schema do
        title "Seasons"
        description "A collection of Seasons"
        type :array
        items Schema.ref(:Season)
      end,
      League: swagger_schema do
        title "League"
        description "A league"
        properties do
          id :integer, "league ID"
          name :string, "league name"
          season Schema.ref(:Seasons), "list of all season"
        end
      end,
      Leagues: swagger_schema do
        title "Leagues"
        description "A collection of Leagues"
        type :array
        items Schema.ref(:League)
        example %{
          data: [
            %{
              id: 1,
              name: "SP1",
              season: [
                %{
                  id: 1,
                  season_name: "201617"
                }
              ]
            }
          ]
        }
      end,
      Error: swagger_schema do
        title "Error"
        description "Error responses from the API"
        properties do
          reason :string, "The message of the error raised", required: true
        end
        example %{
          reason: "Season not found"
        }
      end
    }
  end
end

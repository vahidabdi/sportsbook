defmodule SportsbookWeb.FallbackController do
  use SportsbookWeb, :controller

  alias SportsbookWeb.ErrorView

  def call(conn, {:error, :season_not_found}) do
    conn
    |> put_status(:not_found)
    |> put_resp_content_type("application/json")
    |> render(ErrorView, "404.json", %{reason: :season_not_found})
  end
end

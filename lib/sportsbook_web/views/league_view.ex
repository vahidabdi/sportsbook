defmodule SportsbookWeb.LeagueView do
  use SportsbookWeb, :view

  alias SportsbookWeb.LeagueView
  alias SportsbookWeb.SeasonView

  def render("index.json", %{
        leagues: leagues,
        page_number: page_number,
        page_size: page_size,
        total_pages: total_pages,
        total_entries: total_entries
      }) do
    %{
      data: render_many(leagues, LeagueView, "show.json"),
      page_number: page_number,
      page_size: page_size,
      total_pages: total_pages,
      total_entries: total_entries
    }
  end

  def render("show.json", %{league: league}) do
    %{
      id: league.id,
      name: league.name,
      season: render_many(league.season, SeasonView, "show.json")
    }
  end
end

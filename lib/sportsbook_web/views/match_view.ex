defmodule SportsbookWeb.MatchView do
  use SportsbookWeb, :view

  alias SportsbookWeb.MatchView

  def render("index.json", %{
        matches: matches,
        page_number: page_number,
        page_size: page_size,
        total_pages: total_pages,
        total_entries: total_entries
      }) do
    %{
      data: render_many(matches, MatchView, "show.json"),
      page_number: page_number,
      page_size: page_size,
      total_pages: total_pages,
      total_entries: total_entries
    }
  end
  def render("show.json", %{match: match}) do
    %{
      id: match.id,
      season_id: match.season_id,
      home_team: match.home_team,
      away_team: match.away_team,
      date: match.date,
      fthg: match.fthg,
      ftag: match.ftag,
      hthg: match.hthg,
      htag: match.htag,
      ftr: match.ftr,
      htr: match.htr
    }
  end
end

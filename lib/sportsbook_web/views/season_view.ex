defmodule SportsbookWeb.SeasonView do
  use SportsbookWeb, :view

  alias SportsbookWeb.MatchView

  def render("show.json", %{season: %{matches: %Ecto.Association.NotLoaded{}} = season}) do
    %{
      id: season.id,
      season_name: season.season_name
    }
  end
  def render("show.json", %{season: season}) do
    %{
      id: season.id,
      season_name: season.season_name,
      matches: render_many(season.matches, MatchView, "show.json")
    }
  end
end

defmodule SportsbookWeb.Router do
  use SportsbookWeb, :router
  alias SportsbookWeb.Plug.Format

  pipeline :api do
    plug :accepts, ["json", "protobuf"]
    plug Format
  end

  scope "/api", SportsbookWeb do
    pipe_through :api

    get "/competitions/", CompetitionController, :index
    get "/competitions/:season_id", CompetitionController, :show
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Sportsbook",
      }
    }
  end
  scope "/swagger" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :sportsbook,
      swagger_file: "swagger.json",
      disable_validator: true
  end
end

defmodule SportsbookWeb.Plug.Format do
  @formats Application.get_env(:plug, :formats)
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    [accept] = get_req_header(conn, "accept")
    format = Map.fetch(@formats, accept)
    format_call(conn, format)
  end

  defp format_call(conn, {:ok, [format]}) do
    assign(conn, :format, format)
  end
  defp format_call(conn, _) do
    conn
    |> send_resp(406, "Invalid format")
    |> halt()
  end
end

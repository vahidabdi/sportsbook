#!/bin/bash

/app/bin/sportsbook migrate
/app/bin/sportsbook import /app/lib/sportsbook-0.1.0/priv/repo/csv/Data.csv
trap 'exit' INT; /app/bin/sportsbook foreground

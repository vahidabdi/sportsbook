FROM elixir:1.8.2-alpine as builder

WORKDIR /app/
ENV MIX_ENV=prod

COPY mix.exs mix.lock ./
COPY config config
COPY rel rel
COPY lib lib
COPY priv priv

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix do deps.get, deps.compile

RUN mix release --env=prod --verbose

FROM alpine:latest

RUN apk upgrade --no-cache && \
    apk add --no-cache bash openssl

EXPOSE 4000

ENV PORT=4000 \
    MIX_ENV=prod \
    REPLACE_OS_VARS=true \
    SHELL=/bin/sh

WORKDIR /app/

COPY --from=builder /app/_build/prod/rel/sportsbook/releases/0.1.0/sportsbook.tar.gz .
RUN tar xzf sportsbook.tar.gz && rm -f sportsbook.tar.gz

RUN chown -R root ./releases

USER root

COPY boot.sh boot.sh

ENTRYPOINT ["/app/boot.sh"]
defmodule SportsbookWeb.CompetitionControllerTest do
  use SportsbookWeb.ConnCase, async: true

  import Sportsbook.Factory

  alias Sportsbook.ProtocolBuffer.{LeagueItems, League, MatchItems, Match}

  describe "JSON API" do
    setup [:set_json_accept_header]

    test "paginate all leagues", %{conn: conn} do
      insert_list(3, :league)

      conn = get(conn, Routes.competition_path(conn, :index))
      response = json_response(conn, 200)
      assert length(response["data"]) == 3
    end

    test "traversing the leagues", %{conn: conn} do
      insert_list(13, :league)

      conn = get(conn, Routes.competition_path(conn, :index, %{page: 2}))
      response = json_response(conn, 200)
      assert response["page_number"] == 2
      assert length(response["data"]) == 3
    end

    test "paginate all matches", %{conn: conn} do
      league = insert(:league)
      season = List.first(league.season)

      conn = get(conn, Routes.competition_path(conn, :show, season.id))
      response = json_response(conn, 200)
      assert response["total_entries"] == 15
      assert length(response["data"]) == 10
    end

    test "traversing the matches", %{conn: conn} do
      league = insert(:league)
      season = List.first(league.season)

      conn = get(conn, Routes.competition_path(conn, :show, season.id, %{page: 2}))
      response = json_response(conn, 200)
      assert response["page_number"] == 2
      assert length(response["data"]) == 5
    end
  end

  describe "Protocl Buffer API" do
    setup [:set_protobuf_accept_header]

    test "can decode leagues", %{conn: conn} do
      league = insert(:league)

      conn = get(conn, Routes.competition_path(conn, :index))

      [content_type] = get_resp_header(conn, "content-type")
      response = response(conn, 200)
      decoded_response = LeagueItems.decode(response)

      assert conn.assigns.format == :protobuf
      assert String.contains?(content_type, "x-protobuf")
      assert [%League{} = decoded_league] = decoded_response.leagues
      assert decoded_league.id == league.id
    end

    test "can decode matches", %{conn: conn} do
      league = insert(:league)
      season = List.first(league.season)

      conn = get(conn, Routes.competition_path(conn, :show, season.id))
      [content_type] = get_resp_header(conn, "content-type")
      response = response(conn, 200)
      decoded_response = MatchItems.decode(response)

      assert conn.assigns.format == :protobuf
      assert String.contains?(content_type, "x-protobuf")
      assert [%Match{} = decoded_match|_] = decoded_response.items
      assert decoded_match.season_id == season.id
    end
  end

  def set_json_accept_header(%{conn: conn}) do
    %{conn: put_req_header(conn, "accept", "application/json")}
  end
  def set_protobuf_accept_header(%{conn: conn}) do
    %{conn: put_req_header(conn, "accept", "application/x-protobuf")}
  end
end

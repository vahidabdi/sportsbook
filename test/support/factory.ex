defmodule Sportsbook.Factory do
  use ExMachina.Ecto, repo: Sportsbook.Repo

  alias Sportsbook.Competition.{League, Season, Match}

  def league_factory do
    %League{
      name: sequence(:name, &("League - #{&1}")),
      season: build_list(2, :season)
    }
  end

  def season_factory do
    %Season{
      season_name: sequence(:season_name, &("Season - #{&1}")),
      matches: build_list(15, :match)
    }
  end

  def match_factory do
    %Match{
      home_team: sequence(:home_team, &("Home - #{&1}")),
      away_team: sequence(:away_team, &("Away - #{&1}")),
      date: Date.utc_today(),
      fthg: 1,
      ftag: 1,
      hthg: 0,
      htag: 0,
      ftr: "D",
      htr: "D"
    }
  end
end

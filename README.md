# Sportsbook

An API Server exposing two endpoints:

- List all of the leagues and season (JSON/Protocol Buffer format)
- Fetch all results from specific season (JSON/Protocol Buffer format)


## Requirements

- PostgreSQL 11
- Elixir 1.8.2
- Erlang 21.2.6


## Setup

```bash
mix deps.get
mix compile
mix ecto.reset
iex -S mix phx.server
```

## Running Tests

```bash
MIX_ENV=test mix ecto.create
MIX_ENV=test mix ecto.migrate
mix test
```

## API Documentation

This API only accepts the following `accept` header. for everything else returns `HTTP status code: 406`
```
  accept: application/json
  /
  accept: application/x-protobuf
```

Swagger documentation for json format is available at this [page](http://localhost:4000/swagger)
The same endpoint is also used for Protocol Buffer format, but you need
to add `accept: application/x-protobuf` to your request header



## Deployment

### Build Docker Image

```bash
docker build -t <Tag> .
```

### CI/CD

The project is using Gitlab for CI/CD, see [here](./.gitlab-ci.yml)


### Container Registry

After successful test and build the container images will be uploaded [here](https://gitlab.com/vahidabdi/sportsbook/container_registry)


### Running on Kubernetes

Please read the kubernetes deployment [instruction](./deploy/README.md)


## Metrics

Prometheus metrics are set up and will be reported on `/metrics`.
